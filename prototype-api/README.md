Helm charts for the prototype control plane

## Deploy

### prototype-api

You must either be SSHed into the control-plane or have kubectl access thereto.

```
helm install prototype-api ./prototype-api --set image.tag=[TAG]
```
