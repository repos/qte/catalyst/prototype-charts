Helm charts for the prototype control plane

## Deploy

### prototype-ui

You must either be SSHed into the control-plane or have kubectl access thereto.

```
helm install prototype-ui ./prototype-ui --set image.tag=[TAG]
```
