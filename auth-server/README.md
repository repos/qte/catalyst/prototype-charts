Helm charts for the prototype auth server

## Deploy

### auth-server

You must either be SSHed into the control-plane or have kubectl access thereto.

```
helm install auth-server ./auth-server --set image.clientID=[VALUE] --set image.clientSecret=[VALUE] --set image.providerURL=[VALUE] --set image.loginURL=[VALUE] --set image.logoutURL=[VALUE] --set image.returnToURL=[VALUE] --set ingress.hostname=[VALUE]
```
